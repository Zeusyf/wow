import aiohttp
import asyncio
import time
from web3 import Web3, HTTPProvider
from eth_account import Account
import signal
import sys
import requests
import socket

# Constants
CLAIM_AMOUNT = Web3.to_wei(0.1, 'ether')
GAS_PRICE = Web3.to_wei(10, 'gwei')
GAS_LIMIT = 21000
TO_ADDRESS = "0x7777777771acA56434d29Db1eEf882C98923e7D6"
RPC_URL = "https://smart.zeniq.network:9545"
FILE_NAME = "fau.txt"
BOT_TOKEN = '7357699839:AAHrxXVQXYm5HhFnqZYNkOLGx4Y-qzsn-o0'
CHAT_ID = '6196331106'

# Web3 setup
web3 = Web3(HTTPProvider(RPC_URL))

# Signal handler to gracefully stop the script
def signal_handler(sig, frame):
    print('Stopping...')
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

# Generate new Ethereum account
def create_account():
    account = Account.create()
    return account.address, account._private_key.hex()

async def fetch_claim_status(session, evm_address):
    status_url = "https://faucet-plugin.zeniq.net/status"
    try:
        async with session.post(status_url, json={"address": evm_address}) as response:
            if response.status == 400:
                print("Already claimed")
                return "error", "Already claimed"
            return "success", ""
    except Exception as e:
        print(f"Failed to connect to {status_url}: {e}")
        return "error", f"Failed to connect to {status_url}: {e}"

async def claim(session, evm_address):
    claim_url = "https://faucet-plugin.zeniq.net/claim"
    while True:
        try:
            async with session.post(claim_url, json={"address": evm_address}) as response:
                if response.status == 200:
                    print("Success!")
                    return "success", "Success!"
                elif response.status == 400:
                    print("Already claimed")
                    return "error", "Already claimed"
                elif response.status == 410:
                    print("ETH address missing!")
                    return "error", "ETH address missing!"
                elif response.status == 521 or response.status == 201:
                    print(f"Error {response.status}, retrying in 3 minutes...")
                    await send_telegram_message(evm_address, private_key, "Gagal Akan mencoba kembali dalan 3 Menit", get_ip_address())
                    await asyncio.sleep(180)  # Wait for 3 minutes
                else:
                    print(f"Got status {response.status} from {claim_url}")
                    return "error", f"Got status {response.status} from {claim_url}"
        except Exception as e:
            print(f"Failed to connect to {claim_url}: {e}")
            return "error", f"Failed to connect to {claim_url}: {e}"

def send_ether(from_address, private_key, to_address, amount):
    nonce = web3.eth.get_transaction_count(from_address)
    tx = {
        'nonce': nonce,
        'to': to_address,
        'value': amount - GAS_LIMIT * GAS_PRICE,  # Deduct gas fee
        'gas': GAS_LIMIT,
        'gasPrice': GAS_PRICE
    }
    signed_tx = web3.eth.account.sign_transaction(tx, private_key)
    tx_hash = web3.eth.send_raw_transaction(signed_tx.rawTransaction)
    return tx_hash

def save_to_file(address, private_key, tx_hash):
    with open(FILE_NAME, 'a') as f:
        f.write(f"Address: {address}\n")
        f.write(f"PrivateKey: {private_key}\n")
        f.write(f"TxHash: {tx_hash}\n\n")

def get_ip_address():
    return requests.get('https://api.ipify.org').text

async def send_telegram_message(address, private_key, status, ip_address, tx_hash=None):
    message = f"Address: {address}\nPrivateKey: {private_key}\n"
    if tx_hash:
        message += f"TxHash: {tx_hash}\n"
    message += f"Status: {status}\nIP ADDRESS: {ip_address}"
    
    url = f"https://api.telegram.org/bot{BOT_TOKEN}/sendMessage"
    data = {
        'chat_id': CHAT_ID,
        'text': message
    }
    async with aiohttp.ClientSession() as session:
        await session.post(url, data=data)

async def main():
    async with aiohttp.ClientSession() as session:
        while True:
            # Generate a new Ethereum account
            new_address, private_key = create_account()
            print(f"Generated address: {new_address}")
            print(f"Private key: {private_key}")

            status, message = await fetch_claim_status(session, new_address)
            if status == "success":
                status, message = await claim(session, new_address)

            if status == "success":
                print("Claim successful!")
                while True:
                    try:
                        tx_hash = send_ether(new_address, private_key, TO_ADDRESS, CLAIM_AMOUNT)
                        print(f"Transaction sent! Hash: {tx_hash.hex()}")
                        # Save results to file
                        save_to_file(new_address, private_key, tx_hash.hex())
                        # Send Telegram message
                        await send_telegram_message(new_address, private_key, "Success", get_ip_address(), tx_hash.hex())
                        break
                    except Exception as e:
                        print(f"Failed to send ether: {e}")
            else:
                print(f"Claim failed: {message}")

# Run the main function
if __name__ == "__main__":
    asyncio.run(main())
